import string
import secrets

from django.conf import settings
from django.core.mail import send_mail


def get_password_string(length=8):
    """
    Make a new random name to the image
    """
    alphabet = string.ascii_letters + string.digits
    password = ''.join([secrets.choice(alphabet) for i in range(length)])

    return password


def send_user_email(user, notify_admins=False, **kwargs):
    """
    Send email to recently registered user
    """
    subject = "Product Management. User register"
    message =( 
        "You are registered on Product Management platform. "
        f"Your login password is: {kwargs.get('password')}. "
        "Please sign in and change it."
    )
    to_users = [user.email]
    send_mail(
        subject,
        message,
        settings.EMAIL_HOST_USER,
        to_users,
        fail_silently=False,
    )
