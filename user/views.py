from django.contrib.auth.models import User, Group
from rest_framework import viewsets
from rest_framework import permissions

from .serializers import UserSerializer
from .utils import get_password_string, send_user_email


class UserViewSet(viewsets.ModelViewSet):
    """
    Retrieve, create, update and delete to the admin users.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer
    permission_classes = [permissions.IsAuthenticated]

    def perform_create(self, serializer):
        """
        Define a custom creation of a user.
        Set a random password and set as admin
        """
        user = serializer.save()
        group, _ = Group.objects.get_or_create(name="Admin")
        password = get_password_string()
        user.set_password(raw_password=password)
        user.groups.add(group)
        user.save()
        send_user_email(user=user, password=password)
