from django.db import models

class Product(models.Model):
    """
    Model to make the product handling on data base
    """
    sku = models.CharField(max_length=50)
    name = models.CharField(max_length=50)
    brand = models.CharField(max_length=50)
    price = models.DecimalField(max_digits=12, decimal_places=2)
