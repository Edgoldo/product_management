from .models import Product
from rest_framework import generics, viewsets
from rest_framework import permissions
from .serializers import ProductSerializer
from .utils import send_admin_notifications


class ProductViewSet(viewsets.ModelViewSet):
    """
    View to manage the products.
    """
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    permission_classes = [permissions.IsAuthenticated]

    def perform_create(self, serializer):
        """
        Define a custom creation of a product.
        """
        product = serializer.save()
        product_data = product.__dict__
        send_admin_notifications(condition="created", product=product_data)

    def perform_update(self, serializer):
        """
        Define a custom update of a product.
        """
        product = serializer.save()
        product_data = product.__dict__
        send_admin_notifications(condition="updated", product=product_data)

    def perform_destroy(self, product):
        """
        Define a custom destroy of a product.
        """
        product_data = instance.__dict__
        product.delete()
        send_admin_notifications(condition="deleted", product=product_data)


class ProductListView(generics.ListAPIView):
    """
    View to retrieve the list of products.
    """
    queryset = Product.objects.all()
    serializer_class = ProductSerializer


class ProductRetrieveView(generics.RetrieveAPIView):
    """
    View to retrieve the lookup product.
    """
    lookup_field = 'pk'
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
