from django.conf import settings
from django.contrib.auth.models import User
from django.core.mail import send_mail


def send_admin_notifications(**kwargs):
    """
    Send email to the admin users
    """
    subject = "Product Management. Admin notification"
    condition = kwargs.get("condition")
    product = kwargs.get("product")
    message = (
        f"The next product was {condition} on platform: "
        f"{product.get('sku')} - {product.get('name')} - {product.get('price')}"
    )
    to_users = list(User.objects.filter(groups__name="Admin").values_list("email"))
    send_mail(
        subject,
        message,
        settings.EMAIL_HOST_USER,
        to_users,
        fail_silently=False,
    )