from django.contrib import admin
from django.urls import include, path
from rest_framework import routers
from product.views import (
    ProductViewSet, ProductListView,
    ProductRetrieveView
)
from user.views import UserViewSet


router = routers.DefaultRouter()
router.register(r'users', UserViewSet)
router.register(r'products', ProductViewSet)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('product/list/', ProductListView.as_view(), name='product-list'),
    path('product/retrieve/<int:pk>', ProductRetrieveView.as_view(), name='product-retrieve'),
]
