# Product Management

This system allows to the administrators users create, update an delete products, at the same time anonymous users can show the registered products.

1) Requirements
---------------

This project runs on Linux, it requires the last verstion of python and pipenv, to install pipenv, can use the next command:

$ python3 -m pip install pipenv --upgrade

Then install the requirements of the environment, to this project we use python 3.8:

$ pipenv install

After that, launch the environment of the project:

$ pipenv shell
(product_management) $

2) Run the project
------------------

To start the project on a server, you need to setting the database values:

On the product_management folder, you must rename the default_settings.py to settings.py and then go to the DATABASES variable:

    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'DATABASE_NAME',
        'USER': 'DATABASE_USER',
        'PASSWORD': 'DATABASE_PASSWORD',
        'HOST': 'localhost',
        'PORT': '5432',
        'ATOMIC_REQUESTS': True, # Create transactions on each view request
    }

Change the value of the keys NAME, USER and PASSWORD to your own database configurations.

Then can runs the django commands to migrate the database:

(product_management) $ python manage.py makemigrations product user
(product_management) $ python manage.py migrate

And finally you can run the server project:

(product_management) $ python manage.py runserver

3) Make request to the project urls
-----------------------------------

At this moment the available urls are:

    "users": "http://localhost:8000/users/",
    "user-rud": "http://localhost:8000/users/<int:pk>",
    "products": "http://localhost:8000/products"
    "product-rud": "http://localhost:8000/products/<int:pk>"
    "product-list": http://localhost:8000/product/list/",
    "product-retrieve": http://localhost:8000/product/retrieve/<int:pk>",